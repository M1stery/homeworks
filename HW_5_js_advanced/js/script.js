class TwitterPost {
  constructor(name, username, email, title, body, id) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.title = title;
    this.body = body;
    this.id = id;
  }
  renderPost() {
    return `<div class="twitter" id= ${this.id}>
            <div class="twitter-upper-block">
            <p class="twitter-name">${this.name}</p>
            <p class="twitter-username">${this.username}</p>
            <p class="twitter-email">${this.email}</p>
            <button onclick=" postDelete(${this.id})" id=${this.id} class="post-remove">Delete post</button>
          <button onclick="postEdit(${this.id})" id=${this.id} class="post-edit">Edit post</button>
            </div>
            <div class="twitter-bottom-block">
            <p class="twitter-title">${this.title}</p>
            <p class="twitter-text">${this.body}</p>
            </div>
            </div>`;
  }
  toAdd(position) {
    document.querySelector(`.${position}`).innerHTML += this.renderPost();
  }
}

async function userRqst() {
  const loader = document.createElement("div");
  loader.classList.add("loader");
  document.body.appendChild(loader);

  try {
    const getUsers = await fetch("https://ajax.test-danit.com/api/json/users");
    const users = await getUsers.json();
    const getPosts = await fetch("https://ajax.test-danit.com/api/json/posts");
    const posts = await getPosts.json();
    const postsAboutUser = posts.map((item) => {
      let userInfo = users.find((e) => {
        return e.id == item.userId;
      });
      return {
        ...item,
        userInfo: { ...userInfo },
      };
    });

    postsAboutUser.forEach((element) => {
      const test = new TwitterPost(
        element.userInfo.name,
        `@${element.userInfo.username}`,
        element.userInfo.email,
        element.title,
        element.body,
        element.id
      );
      return test.toAdd("main");
    });
  } catch (error) {
    console.error(error);
  } finally {
    loader.remove();
  }
}

function postEdit(id) {
  const post = document.getElementById(id);
  const postTitle = post.querySelector(".twitter-title").innerText;
  const postText = post.querySelector(".twitter-text").innerText;

  const formHtml = `
    <form id="edit-post-form">
      <label for="post-title">Новий заголовок:</label>
      <input type="text" id="post-title" name="post-title" value="${postTitle}">
      <br>
      <label for="post-text">Новий текст:</label>
      <textarea id="post-text" name="post-text">${postText}</textarea>
      <br>
      <button type="submit">Зберігти</button>
    </form>
  `;

  const formContainer = document.createElement("div");
  formContainer.innerHTML = formHtml;

  post.querySelector(".twitter-bottom-block").appendChild(formContainer);

  const editForm = document.getElementById("edit-post-form");
  editForm.addEventListener("submit", function (event) {
    event.preventDefault();
    savePost(id);
  });
}

async function savePost(id) {
  const post = document.getElementById(id);
  const postTitleInput = document.getElementById("post-title");
  const postTextInput = document.getElementById("post-text");

  const updatedPost = {
    title: postTitleInput.value,
    body: postTextInput.value,
  };

  const response = await fetch(
    `https://ajax.test-danit.com/api/json/posts/${id}`,
    {
      method: "PUT",
      body: JSON.stringify(updatedPost),
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  if (response.ok) {
    const updatedPostData = await response.json();
    const updatedPostHtml = `
      <p class="twitter-title">${updatedPostData.title}</p>
      <p class="twitter-text">${updatedPostData.body}</p>
    `;
    post.querySelector(".twitter-bottom-block").innerHTML = updatedPostHtml;
  }
}

function postDelete(id) {
  const byId = document.getElementById(`${id}`);
  const requestDelete = fetch(
    `https://ajax.test-danit.com/api/json/posts/${id}`,
    { method: "DELETE" }
  ).then((res) => {
    if (res.status === 200) {
      byId.remove();
    }
  });
}

userRqst();
