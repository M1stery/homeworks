class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  set name(value) {
    if (Number(value)) {
      console.log("Error NAME ! Сheck if the input is correct !");
    } else {
      this._name = value;
    }
  }
  set age(value) {
    if (value > 100 || isNaN(value) || !Number(value)) {
      console.log("Error AGE ! Сheck if the input is correct !");
    } else {
      this._age = value;
    }
  }
  set salary(value) {
    if (isNaN(value) || !Number(value)) {
      console.log("Error SALARY ! Сheck if the input is correct !");
    } else {
      this._salary = value;
    }
  }
  get showName() {
    console.log(`Імʼя: ${this._name}`);
  }
  get showAge() {
    console.log(`Вік: ${this._age}`);
  }
  get showSalary() {
    console.log(`Зарплатня: ${this._salary}` + " " + "$");
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get showSalary() {
    console.log(`Зарплатня: ${(this.salary = this._salary * 3)}` + " " + "$");
  }
}

let parent = new Employee("Vyacheslav", 24, 40000, "js");
let programmer1 = new Programmer("Anton", 21, 10000, "c++");
let programmer2 = new Programmer("Victor", 18, 370000, "python");
let programmer3 = new Programmer("Danil", 27, 30000, "java");
let programmer4 = new Programmer("Maksim", 32, 70000, "go");

console.log(parent);
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);
