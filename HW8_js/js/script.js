/*
1.Опишіть своїми словами що таке Document Object Model (DOM).
1.1. Це дерево-видна структура для взаємодії html,css та js.

2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
2.2. innerHTML Повертає всі дочірні елементи, вкладені всередині тега (тег HTML + текстовий вміст). 
innerText Повертає текстовий вміст дочірніх елементів, вкладених усередині мітки.

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
3.3. querySelectorAll,querySelector, getElementById, getElementByTagName, getElementByClassName. 
Якщо нам потрібна жива колекція, тоді це майде усі звернення getElementBy... але я використовую querySelector,
і лише для id - getElementById
*/

let currentP = document.querySelectorAll("p");
currentP.forEach(function (elem) {
  elem.style.background = "#ff0000";
});

let optList = document.getElementById("optionsList");
console.log(optList);
console.log(optList.parentNode);
console.log(optList.childNodes);

let myParagraph = document.createElement("p");
myParagraph.className = "testParagraph";
myParagraph.innerHTML = "This is a paragraph";
console.log(myParagraph);

let headerElements = document.querySelector(".main-header");
console.log(headerElements);

for (const i of headerElements.children) {
  i.classList.add("nav-item");
  console.log(i);
}

let sectionTitle = document.querySelectorAll(".section-title");
console.log(sectionTitle);
for (const i of sectionTitle) {
  i.classList.remove("section-title");
  console.log(i);
}

// let sectionTitle = document.querySelectorAll(".section-title");
// console.log(sectionTitle);
// sectionTitle.forEach((element) => {
//   element.classList.remove("section-title");
// });
