/*
1.Опишіть своїми словами як працює метод forEach.
1.1. Метод forEach() виконує функцію callback один раз для кожного елемента, 
що знаходиться в масиві в порядку зростання.

2.Як очистити масив?
2.2.array.length = 0; array = [1,2,3]/ array = []

3.Як можна перевірити, що та чи інша змінна є масивом?
3.3.Метод Array. isArray() повертає true , якщо об'єкт є масивом і false , якщо він не є масивом
*/
function filterBy(array, type) {
  return array.reduce((previousValue, currentItem) => {
    if (typeof currentItem != type) {
      previousValue.push(currentItem);
    }
    return previousValue;
  }, []);
}

console.log(
  filterBy(["hello", "world", 23, 98, 120, "Vyachelsav", "23", null], "number")
);
