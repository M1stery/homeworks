/*
1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
1.1. Це повтор символа для його використання як строки, тобто якщо нам потрібно написати исмвол /, 
його потрібно буде поторити ще раз, написати //

2.Які засоби оголошення функцій ви знаєте?
2.2. Function daclaration, function expression, arrow fucntion

3.Що таке hoisting, як він працює для змінних та функцій?
3.3. hoisting — це механізм, в якому змінні та оголошення функцій, 
пересуваються вгору своєї області видимості перед тим, як код буде виконано.

*/
function createNewUser() {
  this.firstname = prompt("Set your name");
  this.lastname = prompt("Set your last name");
  this.birthday = prompt("Set birthday (like dd.mm.yyyy)");
  this.getAge = () => {
    return (
      ((new Date().getTime() - new Date(this.birthday).getTime()) /
        (24 * 3600 * 365.25 * 1000)) |
      0
    );
  };
  this.getPassword = () => {
    return (
      this.firstname.charAt(0).toUpperCase() +
      this.lastname.toLowerCase() +
      this.birthday.split(".")[2]
    );
  };
  this.getLogin = () => {
    let getLogin =
      this.firstname[0].toLowerCase() + this.lastname.toLowerCase();
    return getLogin;
  };
}
let newUser = new createNewUser();
Object.defineProperties(newUser, {
  firstname: {
    writable: false,
  },
  lastname: {
    writable: false,
  },
});
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
