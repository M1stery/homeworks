const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class createRoot {
  constructor(elementName, id) {
    this.elementName = elementName;
    this.id = id;
    this.block;
    this.position;
  }
  currentCreate() {
    this.block = document.createElement(this.elementName);
    this.block.id = this.id;
    return this.block;
  }
  addTo(positionOnPage) {
    this.position = document.querySelector(positionOnPage);
    this.position.append(this.block);
    return this.position;
  }
}

class createList extends createRoot {
  constructor(elementName, className) {
    super(elementName);
    this.className = className;
  }
  currentCreate() {
    this.block = document.createElement(this.elementName);
    this.block.className = this.className;
    return this.block;
  }
}

class createArray {
  constructor(processingArray) {
    this.processingArray = processingArray;
    this.finalyArray = [];
  }
  getKeys(arr) {
    for (const keys of arr) {
      try {
        if (keys.name && keys.author && keys.price) {
          for (let i in keys) {
            this.finalyArray.push(`${i}: ${keys[i]}`);
          }
        } else {
          throw new Error(
            "Ops ! Can`t declarate undefined, any of keys have none parametrs"
          );
        }
      } catch (err) {
        console.log(err.message);
      }
    }
    return this.finalyArray;
  }

  pushToHTML(parentE, element) {
    let ul = document.querySelector(parentE);
    this.finalyArray.forEach((e) => {
      let li = document.createElement(element);
      ul.append(li);
      li.innerText = e;
    });
  }
}

let div = new createRoot("div", "root");
div.currentCreate();
div.addTo("body");
let ul = new createList("ul", "books-list");
ul.currentCreate();
ul.addTo("#root");
let array = new createArray(books);
array.getKeys(books);
array.pushToHTML(".books-list", "li");
