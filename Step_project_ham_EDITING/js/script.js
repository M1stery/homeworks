"use strict";
// Our Services Tabs

const tabItems = document.querySelectorAll(".tabs-items");
const tabsContent = document.querySelectorAll(".tabs-text");
const tabsImg = document.querySelectorAll(".tabs-img");

tabItems.forEach((element) => {
  element.addEventListener("click", () => {
    let currentBtn = element;
    let tabId = currentBtn.getAttribute("data-tab");
    let currentTab = document.querySelectorAll(tabId);
    tabItems.forEach(function (item) {
      item.classList.remove("tab-active");
    });
    tabsContent.forEach(function (item) {
      item.classList.add("hidden");
    });

    tabsImg.forEach(function (item) {
      item.classList.add("hidden");
    });

    currentBtn.classList.add("tab-active");
    currentTab.forEach(function (e) {
      e.classList.remove("hidden");
    });
  });
});

// Our Amazing Block Filter + Button
let allTabs = document.querySelectorAll(".filter-tabs-content");
const loadMore = document.querySelector(".filter-tabs-btn");
const filterTabs = document.querySelector(".filter-tabs");
let blockHolder = document.querySelector(".block-holder");
let contentCounter = 12;
let divsContent;
const divsArray = [];
const mainAmazingBlock = document.querySelector(".main-amazing-tabs-content");
const set13 = ["all", "wordpress", "filter-tabs-content", "content-13"];
const set14 = ["all", "wordpress", "filter-tabs-content", "content-14"];
const set15 = ["all", "wordpress", "filter-tabs-content", "content-15"];
const set16 = ["all", "landing-pages", "filter-tabs-content", "content-16"];
const set17 = ["all", "landing-pages", "filter-tabs-content", "content-17"];
const set18 = ["all", "landing-pages", "filter-tabs-content", "content-18"];
const set19 = ["all", "web-design", "filter-tabs-content", "content-19"];
const set20 = ["all", "web-design", "filter-tabs-content", "content-20"];
const set21 = ["all", "web-design", "filter-tabs-content", "content-21"];
const set22 = ["all", "graphic-design", "filter-tabs-content", "content-22"];
const set23 = ["all", "graphic-design", "filter-tabs-content", "content-23"];
const set24 = ["all", "graphic-design", "filter-tabs-content", "content-24"];

loadMore.addEventListener("click", () => {
  for (let index = 0; index < contentCounter; index++) {
    divsContent = document.createElement("div");
    divsArray.push(divsContent);
  }
  divsArray.forEach((element) => {
    filterTabs.appendChild(element);
  });
  set13.forEach((e) => {
    divsArray[0].classList.add(e);
  });
  set14.forEach((e) => {
    divsArray[1].classList.add(e);
  });
  set15.forEach((e) => {
    divsArray[2].classList.add(e);
  });
  set16.forEach((e) => {
    divsArray[3].classList.add(e);
  });
  set17.forEach((e) => {
    divsArray[4].classList.add(e);
  });
  set18.forEach((e) => {
    divsArray[5].classList.add(e);
  });
  set19.forEach((e) => {
    divsArray[6].classList.add(e);
  });
  set20.forEach((e) => {
    divsArray[7].classList.add(e);
  });
  set21.forEach((e) => {
    divsArray[8].classList.add(e);
  });
  set22.forEach((e) => {
    divsArray[9].classList.add(e);
  });
  set23.forEach((e) => {
    divsArray[10].classList.add(e);
  });
  set24.forEach((e) => {
    divsArray[11].classList.add(e);
  });

  allTabs = document.querySelectorAll(".all");

  divsArray.forEach((elements) => {
    let copyHolder = blockHolder.cloneNode(true);
    elements.appendChild(copyHolder);
  });

  blockHolderText.forEach((e) => {
    emptyBlock.push(e);
  });
  emptyBlock.forEach((e) => {
    emptyBlock.splice(0);
    emptyBlock = e.innerHTML = "";
    allTabs.forEach((e) => {
      if (e.classList.contains("graphic-design")) {
        emptyBlock.innerHTML = "Graphi";
      }
    });
  });
});
let test;
let emptyBlock = [];
let blockHolderText = document.querySelectorAll(".block-holder-text");

// function checkText() {

// }

document
  .querySelector(".main-amazing-tabs-content")
  .addEventListener("click", (event) => {
    if (event.target.tagName !== "LI") return false;
    let filterClass = event.target.dataset["filter"];
    allTabs.forEach((elem) => {
      elem.classList.remove("hidden");
      if (!elem.classList.contains(filterClass)) {
        elem.classList.add("hidden");
      }
    });
  });

loadMore.addEventListener("click", () => {
  loadMore.classList.add("hidden");
});

//Our Amazing Work Focus Tabs

const tabsFocus = document.querySelectorAll(".main-amazing-tabs-items");

tabsFocus.forEach((items) => {
  items.addEventListener("click", () => {
    tabsFocus.forEach((items) => {
      items.classList.remove("amazing-tabs-items-active");
    });
    items.classList.add("amazing-tabs-items-active");
  });
});

// What people say about theHam slider

const nextArrow = document.querySelector(".nextArrow");
const previousArrow = document.querySelector(".previousArrow");
let i;
let n;
const slides = document.getElementsByClassName("people-theham-person");
const dots = document.getElementsByClassName("people-theham-person-small");
const peopleNames = document.getElementsByClassName("people-theham-names");
const peopleText = document.getElementsByClassName("people-theham-text");
const peopleJobTitle = document.getElementsByClassName(
  "people-theham-jobtitle"
);

let slideIndex = 1;
showSlides(slideIndex);
function plusSlide() {
  n = 1;
  showSlides((slideIndex += n));
}

function minusSlide() {
  n = 1;
  showSlides((slideIndex -= n));
}

function currentSlide() {
  showSlides((slideIndex = n));
}

function showSlides(n) {
  if (n > slides.length) {
    slideIndex = 1;
  }

  if (n < 1) {
    slideIndex = slides.length;
  }

  for (i = 0; i < slides.length; i++) {
    slides[i].classList.add("hidden");
  }

  for (i = 0; i < peopleNames.length; i++) {
    peopleNames[i].classList.add("hidden");
  }

  for (i = 0; i < peopleText.length; i++) {
    peopleText[i].classList.add("hidden");
  }
  for (i = 0; i < peopleJobTitle.length; i++) {
    peopleJobTitle[i].classList.add("hidden");
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].classList.remove("people-theham-position");
  }

  slides[slideIndex - 1].classList.remove("hidden");
  dots[slideIndex - 1].classList.add("people-theham-position");
  peopleText[slideIndex - 1].classList.remove("hidden");
  peopleNames[slideIndex - 1].classList.remove("hidden");
  peopleJobTitle[slideIndex - 1].classList.remove("hidden");
}
nextArrow.addEventListener("click", plusSlide);
previousArrow.addEventListener("click", minusSlide);

// checkText();
