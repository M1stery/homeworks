async function getIp() {
  const ipBtn = document.querySelector(".ip-btn");
  const url = "https://api.ipify.org/?format=json";
  const ipResearch = `http://ip-api.com/json/`;

  ipBtn.addEventListener("click", async () => {
    const localRequest = await fetch(url);
    const localIp = await localRequest.json();
    const clientIp = localIp.ip;
    const researchRequest = await fetch(`${ipResearch}${clientIp}`);
    const researchData = await researchRequest.json();
    return document.body.insertAdjacentHTML(
      `beforeend`,
      `<div class="info-client__ip">
      <p class="continent">Континент: ${researchData.timezone}</p>
      <p class="country">Країна: ${researchData.country}</p>
      <p class="region">Регіон: ${researchData.region}</p>
      <p class="city">Місто: ${researchData.city}</p>
      <p class="city">Код країни: ${researchData.countryCode}</p>
    </div>`
    );
  });
}

getIp();
