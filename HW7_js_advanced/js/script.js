class Game {
  constructor() {
    this.score = {
      user: 0,
      computer: 0,
    };
    this.difficulty = null;
    this.gameOver = false;
    this.gameBoard = document.getElementById("game-board");
    this.cells = [];
    this.intervalId = null;
    this.userClickHandler = this.userClickHandler.bind(this);
    this.startGame = this.startGame.bind(this);
    this.endGame = this.endGame.bind(this);
  }

  createBoard() {
    const fragment = document.createDocumentFragment();
    for (let i = 0; i < 10; i++) {
      const row = document.createElement("tr");
      for (let j = 0; j < 10; j++) {
        const cell = document.createElement("td");
        row.appendChild(cell);
        this.cells.push(cell);
      }
      fragment.appendChild(row);
    }
    this.gameBoard.appendChild(fragment);
  }

  highlightRandomCell() {
    const randomIndex = Math.floor(Math.random() * this.cells.length);
    const cell = this.cells[randomIndex];
    cell.style.backgroundColor = "blue";
    setTimeout(() => {
      cell.style.backgroundColor = "";
    }, this.getDifficultyInterval());
  }

  getDifficultyInterval() {
    const intervals = {
      easy: 1500,
      medium: 1000,
      hard: 500,
    };
    return intervals[this.difficulty] || intervals.medium;
  }

  userClickHandler(event) {
    if (event.target.style.backgroundColor === "blue") {
      event.target.style.backgroundColor = "green";
      this.score.user++;
    } else {
      this.score.computer++;
      event.target.style.backgroundColor = "red";
    }
    if (this.checkGameOver()) {
      this.endGame();
    }
  }

  checkGameOver() {
    const coloredCells = this.cells.filter(
      (cell) =>
        cell.style.backgroundColor === "green" ||
        cell.style.backgroundColor === "red"
    );
    return coloredCells.length >= this.cells.length / 2;
  }

  endGame() {
    this.gameOver = true;
    clearInterval(this.intervalId);
    const message =
      this.score.user > this.score.computer
        ? "You Win!"
        : this.score.user < this.score.computer
        ? "Computer Wins!"
        : "It's a tie!";
    alert(`Game Over. ${message}`);
  }

  startGame(difficulty) {
    this.difficulty = difficulty;
    this.score.user = 0;
    this.score.computer = 0;
    this.gameOver = false;
    this.cells.forEach((cell) => (cell.style.backgroundColor = ""));
    clearInterval(this.intervalId);
    this.intervalId = setInterval(() => {
      this.highlightRandomCell();
    }, this.getDifficultyInterval());
  }
}

const game = new Game();
game.createBoard();
game.gameBoard.addEventListener("click", game.userClickHandler);

const startButtons = document.querySelectorAll(".start-button");
startButtons.forEach((button) => {
  button.addEventListener("click", (event) => {
    const difficulty = event.target.dataset.difficulty;
    game.startGame(difficulty);
  });
});
