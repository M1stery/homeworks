/*
1.Опишіть, як можна створити новий HTML тег на сторінці.
1.1. Через createElement('...')

2.Опишіть, що означає перший параметр функції insertAdjacentHTML 
і опишіть можливі варіанти цього параметра.
2.2. Перший параметр означає позицію запису відносно елемента. 
beforebegin: до самого element (до відкриваючого тега).
afterbegin: відразу після тега element, що відкриває (перед першим нащадком).
beforeend: відразу перед закриваючим тегом element (після останнього нащадка).
afterend: після element (після закриває тега).

3.Як можна видалити елемент зі сторінки?
3.3. Через remove() або ''
*/

const currentArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const currentElement = "p";

function arrayConverter(arr, element) {
  arr.forEach((i) => {
    let li = document.createElement("li");
    li.innerHTML = i;
    let search = document.querySelector(element);
    search.append(li);
  });
}

arrayConverter(currentArray, currentElement);
