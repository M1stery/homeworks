const tabs = document.querySelectorAll(".tabs-title");
const content = document.querySelectorAll(".content");

for (let i = 0; i < tabs.length; i++) {
  tabs[i].addEventListener("click", (event) => {
    console.log(event.target);

    let currentTabs = event.target.parentElement.children;
    for (t = 0; t < currentTabs.length; t++) {
      currentTabs[t].classList.remove("active");
    }

    event.target.classList.add("active");

    let currentContent = event.target.parentElement.nextElementSibling.children;
    for (c = 0; c < currentContent.length; c++) {
      console.log(currentContent[c]);
      currentContent[c].classList.remove("visible");
      currentContent[c].classList.add("hidden");
    }

    content[i].classList.add("visible");
  });
}
