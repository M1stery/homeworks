/*
1.Опишіть своїми словами, що таке метод об'єкту.
    1.1. Це функція яка створена всередені об'єкту, дія яку можна виконати з об'єктом.

2.Який тип даних може мати значення властивості об'єкта?
    2.2. Значення властивості може приймати будь-який тип даних.

3.Об'єкт це посилальний тип даних. Що означає це поняття?
    3.3. Значенням посилального типу є посилання на якесь значення.

*/

function createNewUser() {
  this.firstname = prompt("Set your name");
  this.lastname = prompt("Set your last name");
  this.getLogin = () => {
    let getLogin =
      this.firstname.charAt(0).toLowerCase() + this.lastname.toLowerCase();
    return getLogin;
  };
}
let newUser = new createNewUser();
Object.defineProperties(newUser, {
  firstname: {
    writable: false,
  },
  lastname: {
    writable: false,
  },
  setFirstName(value) {
    newUser.firstname = value;
  },
});
console.log(newUser);
console.log(newUser.getLogin());
newUser.firstname = "Alice";
newUser.lastname = "Trooper";
console.log(newUser.firstname);
console.log(newUser.lastname);
