const btnChange = document.querySelector(".btn-theme-change");
const allText = document.querySelectorAll(".change-theme");
const btnFooter = document.querySelector(".button-footer");
const allHeader = document.querySelectorAll(".header-links");

const currentTheme = localStorage.getItem("theme");

if (currentTheme == "dark") {
  document.body.classList.add("dark-theme");
  btnFooter.classList.add("button-footer-change");
  allText.forEach((e) => {
    e.classList.add("change-theme-dark");
  });
  allHeader.forEach((e) => {
    e.classList.add("header-links-change");
  });
  btnChange.classList.add("btn-theme-change-dark");
}

btnChange.addEventListener("click", () => {
  document.body.classList.toggle("dark-theme");
  allText.forEach((e) => {
    e.classList.toggle("change-theme-dark");
  });
  btnFooter.classList.toggle("button-footer-change");
  allHeader.forEach((e) => {
    e.classList.toggle("header-links-change");
  });
  btnChange.classList.toggle("btn-theme-change-dark");
  let theme = "light";
  if (document.body.classList.contains("dark-theme")) {
    theme = "dark";
  }

  localStorage.setItem("theme", theme);
});
