function dropDownMenu() {
  const dropdown = document.querySelector(".dropdown");
  const dropdownImg = document.querySelectorAll(".dropdown__image");
  const dropdownItems = document.querySelectorAll(".dropdown-menu__items");
  dropdown.addEventListener("click", () => {
    const dropdownSlide = document.querySelector(".dropdown-menu");
    dropdownSlide.classList.toggle("dropdown-menu--hidden");
    dropdownImg.forEach((e) => {
      e.classList.toggle("dropdown__image--hidden");
    });
  });
}

dropDownMenu();
