const button = document.querySelector(".btn");
const eyeIcon = document.querySelectorAll(".icon-password");
const inputRepeat = document.getElementById("input-two");
const inputPass = document.getElementById("input-one");

eyeIcon.forEach((e) => {
  e.addEventListener("click", () => {
    e.classList.toggle("fa-eye-slash");
    const inputType = e.previousElementSibling.getAttribute("type");
    if (inputType === "password") {
      e.previousElementSibling.setAttribute("type", "text");
    } else {
      e.previousElementSibling.setAttribute("type", "password");
    }
  });
});

function comparePass() {
  return inputPass.value === inputRepeat.value;
}

button.addEventListener("click", (event) => {
  event.preventDefault();
  const error = document.querySelector(".error");
  const currentCompare = comparePass();
  if (currentCompare) {
    // document.querySelector(".error").style.display = "none";
    alert("You are welcome");
    error.style.display = "none";
  } else {
    error.style.display = "inline-block";
  }
});
