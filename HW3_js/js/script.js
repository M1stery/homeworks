"use strict";
let userNumber = +prompt("Введите своё число");
while (isNaN(userNumber) || !Number.isInteger(userNumber)) {
  userNumber = +prompt("Это не число ! Введите число");
}

for (let i = 0; i <= userNumber; i++) {
  if (i % 5 === 0) {
    console.log(i);
  } else if (userNumber <= 1 && userNumber <= 4) {
    console.log("Sorry, no numbers");
  }
}
