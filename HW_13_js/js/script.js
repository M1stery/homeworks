/*
1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?


1.1. setTimeout дозволяє викликати функцію один раз через певний проміжок часу. 
setInterval дозволяє викликати функцію, що повторюється через певний проміжок часу

2.2. Миттєво вона не старацює , тому-що спочатку виновається основне тіло коду,а потім таймаут. Так виклик функції буде заплановано одразу після виконання поточного коду

3.3. Для того щоб наприклад повністью видалити з памʼяті виклик функції
*/

const images = ["./img/1.jpg", "./img/2.jpg", "./img/3.JPG", "./img/4.png"];
let imgLenght = images.length;
let index = 1;
const stopBtn = document.querySelector(".btn-stop");
const startBtn = document.querySelector(".btn-start");
let imgInterval = setInterval(imgArrayStart, 3000);

function imgArrayStart() {
  if (index == length) index = 0;
  if (index == 4) {
    index = 0;
  }
  document.querySelector(".image-to-show").src = images[index++];
}

setInterval(function () {
  stopBtn.classList.remove("hide");
}, 6000);

stopBtn.addEventListener("click", () => {
  clearInterval(imgInterval);
  startBtn.classList.remove("hide");
});

startBtn.addEventListener("click", () => {
  imgInterval = setInterval(imgArrayStart, 3000);
});
