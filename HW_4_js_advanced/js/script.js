fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    data.forEach((e) => {
      let films = document.querySelector(".films");
      films.innerHTML += `<div id='num${e.episodeId}'><p class='films-items'>${e.episodeId}</p>
      <p class='films-items'>${e.name}</p>
      <p class='films-items'>${e.openingCrawl}</p></div>`;

      e.characters.forEach((el) => {
        console.log(el);
        fetch(el)
          .then((response) => response.json())
          .then((response) => {
            document.querySelector(
              `#num${e.episodeId}`
            ).innerHTML += `<p>${response.name}</p>`;
          });
      });
    });
  });
